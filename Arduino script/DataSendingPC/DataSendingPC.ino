
#include <SPI.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <SD.h>


int status = WL_IDLE_STATUS;
#include "arduino_secrets.h" 
///////please enter your sensitive data in the Secret tab/arduino_secrets.h

bool wifi_type_wpa = WIFI_TYPE_WPA;

char ssid_WPA[] = SECRET_SSID_WPA;        // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key index number (needed only for WEP)

char ssid_WPA2[] = SECRET_SSID_WPA2;
char username[] = SECRET_USERNAME;
char password[] = SECRET_PASSWORD;


unsigned int localPort = 8080;      // local port to listen for UDP packets

const char* ntpServerName = "ntp11.metas.ch";  // NTP server URL
IPAddress ntpServerIP;  

const int NTP_PACKET_SIZE = 48; // NTP timestamp is in the first 48 bytes of the message

byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
byte mac[6];                     // the MAC address of your Wifi shield

// A UDP instance to let us send and receive packets over UDP
WiFiUDP Udp;

char dateTime[26]; // time at the sycronization with the ntp server in the form yyyy_mm_dd-hh_mm_ss.micros
unsigned long long internalClockStart; // the time in micro seconde from the start of the scritp and the time i sychronize with ntp server. it is 8 byte variable


const char* serverIP = "192.168.146.26";
const unsigned int serverPort = 8080;

#define SDPIN 10
SdFile root;
Sd2Card card;
SdVolume volume;

File myFile;
char filename[] = "File_";

//Variable to dertermine the detection and the coincidence
unsigned int detectionOnXaxes; // Give the information where the detection was made, 0 mean no dection made yet 
unsigned int detectionOnYaxes; 
char detectionAxes[1];

unsigned int triggerDetection = 50; //Value is took as an detection and register
unsigned int tresholdStop = 25; //Value check to concider the detection as fail to coincid
// analogRead take int on 10bit (12bit max) and Voltage detected follow 3.3/1024 per value for 10 bit

static const uint8_t analogPinListX[] = {0,A3,A2,A1,A0};//choice of dispostion on the X axes to determine which detecro is named 1-4 with Apin0-3
static const uint8_t analogPinListY[] = {0,A7,A6,A4,A5};

unsigned long detectionTimeX;
unsigned long detectionTimeY;

unsigned int noDetectorOnX = 4;
unsigned int noDetectorOnY = 4;

void setup() {
  // Open serial communications and wait for port to open:
  Serial.begin(9600);

  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {
    Serial.println("Please upgrade the firmware");
  }

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    // Connect to WPA/WPA2 network.
    if (wifi_type_wpa)
    {
      Serial.println(ssid_WPA);
      status = WiFi.begin(ssid_WPA, pass);
      Serial.println(status);
    }else
    {
      Serial.println(ssid_WPA2);
      status = WiFi.beginEnterprise(ssid_WPA2, username, password);
      Serial.println(status);
    }

    // wait 10 seconds for connection:
    delay(10000);
  }

  Serial.println("Connected to WiFi");
  printWifiStatus();

  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print("NTP server IP: ");
  Serial.println(ntpServerIP);

  Serial.println("Starting connection to server...");
  if (Udp.begin(localPort) == 1) {
    Serial.print("UDP initialized successfully on port ");
    Serial.println(localPort);
  } else {
    Serial.println("Failed to initialize UDP");
  }

  WiFi.macAddress(mac);

  sychronizeClockNTP();

  headerPacket();

}

void loop() {
  for (int i=0; i < noDetectorOnX; i++) {
    detectionTimeX = detection(i+1, "X");
  }
  for (int i=0; i < noDetectorOnY; i++) {
    detectionTimeY = detection(i+1, "Y");
  }

  // unsigned long detectionTime = millis() - internalClockStart;
  // sendData(random(1, 4), random(1, 4), detectionTime);
  if (detectionOnXaxes and detectionOnYaxes) {
    if (detectionTimeX < detectionTimeY){
      sendData(detectionOnXaxes, detectionOnYaxes, detectionTimeX);
    } else {
      sendData(detectionOnXaxes, detectionOnYaxes, detectionTimeY);
    }
    waitResetTwoAnalogPin(analogPinListX[detectionOnXaxes], analogPinListY[detectionOnYaxes]);
  } else if (! detectionOnXaxes and analogRead(analogPinListX[detectionOnXaxes]) > tresholdStop) {
    detectionOnXaxes = 0;
  } else if (! detectionOnYaxes and analogRead(analogPinListY[detectionOnYaxes]) > tresholdStop) {
    detectionOnYaxes = 0;
  }
}

unsigned long detection(unsigned int position, char axes[1]) {
  if (axes == "X") {
    uint8_t analogPin = analogPinListX[position];
    if (analogRead(analogPin) > triggerDetection) {
      unsigned long detectionTime = millis() - internalClockStart;
      if (analogPinListX[detectionOnXaxes] != analogPin && detectionOnXaxes) {
        waitResetTwoAnalogPin(analogPin, analogPinListX[detectionOnXaxes]);
        detectionOnXaxes = 0;
      }
      else {
        detectionOnXaxes = position;
        return detectionTime;
      }
    }
  } else if(axes == "Y") {
    uint8_t analogPin = analogPinListY[position];
    if (analogRead(analogPin) > triggerDetection) {
      unsigned long detectionTime = millis() - internalClockStart;
      if (analogPinListY[detectionOnYaxes] != analogPin && detectionOnYaxes) {
        waitResetTwoAnalogPin(analogPin, analogPinListY[detectionOnYaxes]);
        detectionOnYaxes = 0;
      }
      else {
        detectionOnYaxes = position;
        return detectionTime;
      }

    }
  }
  return 0;
}

void waitResetTwoAnalogPin(uint8_t Pin_1, uint8_t Pin_2) {
  bool secondPinAlreadyReset = false;
  while (analogRead(Pin_1) > tresholdStop ) {
    if (analogRead(Pin_2) > tresholdStop && !secondPinAlreadyReset){secondPinAlreadyReset = true;}
  }
  if (secondPinAlreadyReset) {while(analogRead(Pin_2) > tresholdStop){continue;}}
}

// send an NTP request to the time server at the given address
unsigned long sendNTPpacket(IPAddress& address) {
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void sychronizeClockNTP(){
  sendNTPpacket(ntpServerIP);
  while (Udp.parsePacket() != 48)
  {
    delay(10);
  }
  
  Serial.println("packet received");

  Udp.read(packetBuffer, NTP_PACKET_SIZE);

  internalClockStart = millis();

  unsigned long long secsSince1900 = (unsigned long long)packetBuffer[40] << 24 | (unsigned long long)packetBuffer[41] << 16 |
  (unsigned long long)packetBuffer[42] << 8 | (unsigned long long)packetBuffer[43];

  unsigned long long fraction = (unsigned long long)packetBuffer[44] << 24 | (unsigned long long)packetBuffer[45] << 16 |
  (unsigned long long)packetBuffer[46] << 8 | (unsigned long long)packetBuffer[47];
  
  const unsigned long seventyYears = 2208988800UL;
  unsigned long epoch = secsSince1900 - seventyYears;

  time_t t = epoch;
  
  unsigned long milliseconds = (fraction * 1000) / 0x100000000ULL;
  
  setTime(t);
  sprintf(dateTime, "%04u_%02u_%02u-%02u_%02u_%02u.%03u", year(), month(), day(), hour(), minute(), second(), milliseconds);

  Serial.println(dateTime);

}

void sendPacketToPC (char packetSendPC[32]) {
  Udp.beginPacket(serverIP, serverPort);
  Udp.write(packetSendPC, 32);
  Udp.endPacket();
}

void headerPacket() {
  char header[32];
  char buffer[32];
  memset(header, '-', 31);
  memset(buffer, ' ', 31);
  sprintf(buffer, "Date:%s", dateTime);
  
  sendPacketToPC(header);
  delay(100);
  sendPacketToPC(buffer);
  delay(100);
  sendMacAdress();
  delay(100);
  sendPacketToPC(header);

}

void sendMacAdress() {
  char macStr[32];

  memset(macStr, ' ', 31);
  snprintf(macStr, 32, "mac:%02x:%02x:%02x:%02x:%02x:%02x",mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
  
  sendPacketToPC(macStr);
}

void sendData(uint8_t X, uint8_t Y, unsigned long detectionTime_) {
  char buffer[32];
  memset(buffer, ' ', 32);


  snprintf(buffer, sizeof(buffer), "Time:%u - X:%u Y:%u", detectionTime_, X, Y);
  sendPacketToPC(buffer);

}