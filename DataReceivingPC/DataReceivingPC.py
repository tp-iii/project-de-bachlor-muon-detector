
import socket
import os

HOST = '192.168.146.26'
PORT = 8080

arduino_info = {}
file_base_path = "C:/Users/sebas/Desktop/EPFL/BA5/TP-III/Project de Bachlor - Muon Detector/data"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((HOST, PORT))

print(f"Server listening on {HOST}:{PORT}")

try:
    while True:
        data, addr = sock.recvfrom(32)  # Buffer size is 1024 bytes
        data = data.replace(b'\x00', b' ')
        message = data.decode()
        if "-" in message :
            for i in range(3) :
                data, addr = sock.recvfrom(32)  # Buffer size is 1024 bytes
                data = data.replace(b'\x00', b' ')
                if i == 0 : dataTime_message = data.decode()
                if i == 1 : mac_message = data.decode()
                if i == 2 : end_message = data.decode()

            print(f"{message}\n{dataTime_message}\n{mac_message}\n{end_message}\n")
            date_str = dataTime_message.split(':')[1]
            mac_address = mac_message.split(':')[1]


 
            # Create a new file with the date as the filename
            file_name = date_str[:-4] + ".txt"
            file_path = os.path.join(file_base_path, file_name)
            with open(file_path, "w"):
                pass  # Create an empty file
            arduino_info[addr[0]] = {
                    "mac": mac_address,
                    'file_path': file_path
                }
            
            file = open(file_path, "a")
            file.write(f"{message}\n{dataTime_message}\n{mac_message}\n{end_message}\n")
            file.flush()
            file.close()

        else :
            print(f"Received message: {message} from {addr}")
            file = open(arduino_info.get(mac_address, {}).get('file_path', None), "a")
            file.write(f"{message}\n")
            file.flush()
            file.close()

except KeyboardInterrupt:
    print("Server is shutting down...")

finally:
    sock.close()